# README #

This is repository of Raspberry Pi Useful Shell Command.

## Update/Upgrade command

    sudo apt-get update
    sudo apt-get upgrade
    
## Screen command
### Create screen

    screen

### Get Screen list

    screen -list
    
### kill special screen

    screen -S ScreenNumber -X quit
    ex: screen -S 23534 -X quit

### Back to main terminal from screen

	Press <Ctrl + 'A'>, then <'D'>
	
### Go to screen

	screen -r
	
## File/Directory command
### Directory structure 

    ls
    ls -l
    
### Create directory

    mkdirs
    
### Create file

    touch
    
### Copy file/directory

    cp XXX
    
### Move file/directory

    mv XXX
    
### Delete file

    rm filename 
    
### Delete directory

    rmdir directory

### Fully remove directory

	rm -r directory
	
### Check File Size

	ls -lh filename
	ls -l *										// Size of All the files in the current directory
	ls -al *       								// Size of All the files including hidden files in the current directory
	ls -al dir/    								// Size of All the files including hidden files in the 'dir' directory
	
### Check Directory Size

	du -sh directory_name
    du -bsh *                                   // Check size of all sub-directories 			                         						
	
### Head/Tail 
    
    head filename                               // shows head part of the file
    tail filename                               // shows tail part of the file
                                                
## Networking/Internet command

    ifconfig
    iwconfig
    iwlist
    iwlist wlan0 scan
    nmap
    ping
    
## System Information command

    cat /proc/meminfo
    cat /proc/partitions
    cat /proc/version
    df -h
    df /
    dpkg --get-selections | grep XXX            // shows all of the installed packages that are related to XXX
    free                                        // shows how much free memory is available
    hostname -I                                 // shows the IP address of your Raspberry Pi
    lsusb                                       // lists USB hardware connected to your Raspberry Pi
    vcgencmd measure_temp                       // shows the temperature of the CPU
    vcgencmd get_mem arm                        // shows the arm memory
    vcgencmd get_mem gpu                        // shows the gpu memory
    
## Permission command

    chmod symbol                                // change permissions for a file
        symbol: u, g, o, r, w, x
        u: user that owns the file
        g: the files group
        o: other user
        r: read
        w: write
        x: execute
        
        ex: chmod u+x *filename*
        
    chown                                       // change the user and/or group that owns a file
        ex: sudo chown pi:root *filename*
        
    ssh                                         // denotes the secure shell
    scp                                         // copies a file from one computer to another using ssh
    
## Search command
    
    grep                                        // search inside files for certain search patterns
    awk                                         // programming language useful for searching and manipulating text files
    find                                        // searches a directory and subdirectories for files mathcing certain patterns
    whereis                                     // find the location of a command
	which										
    
## Partition information

	sudo blkid                                  // show partition information
    
## Package information

	pip(pip3) show 'package'					// show package information installed with pip (Name, Version, Summary, Home-page, Author, Author-email, License, Location, Requires)
	
		ex: pip show numpy
		
		Name: numpy
		Version: 1.12.1
		...
		Location: /usr/lib/python2.7/dist-packages
		Requires:
		
	apt show 'package':							// show package information installed with apt (Package, Version, ... Description)
		
	pip freeze | grep 'package'					// show package version
	
	pip list									// show all installed pip package 
	
	apt list									// show all installed apt package
		
## Raspbian information

	cat /etc/os-release
	lsb_release -a                              // this command needs lsb_release package to be installed: "sudo apt-get install lsb-release"
	uname -a
	cat /etc/rpi-issue                          // Raspbian Release Date	
	
## Upgrade numpy 
pip3:

	sudo apt-get remove python3-numpy
	sudo pip3 install --upgrade numpy
	
## Screen Capture command

	sudo apt-get install scrot
	scrot										// should use this command in RPi terminal directly, not using putty
	
## Other command

    pwd                                         // shows the current path
    htop										// task manager
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

    